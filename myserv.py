#!/usr/bin/python
"""
The server in this module manages all application communications. Should
be able to handle multiple disconnects.
"""
import sys
import socket
import errno

import mysock


class MyServerSocket():
    """ This class encapsulates low level server socket stuff. """

    def __init__(self, host, port):
        """ Setup a non-blocking IPv4 stream socket that allows for address
        reuse on death. """
        self._server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._server.setblocking(0) # non-blocking socket
        self._client = None

        # setup the socket s.t. we can reuse the addr it is bound to
        self._server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        # bind the socket to the addr (host, port)
        self._server.bind((host, port))

    def accept(self):
        """ Accept a client connection. Returns true on success. """
        # Since we are using a non-blocking server socket, EAGAIN error
        # is raised when accept returns without accepting a connection,
        # but this error just means try again. Any error other is actually
        # an error.
        try:
            self._server.listen(1)
            (csock, caddr) = self._server.accept()
            self._client = mysock.MySocket(csock)
        except socket.error as err:
            if err[0] == errno.EAGAIN: return False # try again
            else: raise err

        return True

    def mysend(self, msg):
        self._client.mysend(msg)

    def myrecv(self):
        return self._client.myrecv()
        
    def close(self):
        """ Close the server and client sockets if they are not None. """
        if self._server: self._server.close()
        if self._client: self._client.close()

if __name__ == "__main__":
    if len(sys.argv) != 3: sys.exit("usage: myserv.py host port")

    s = MyServerSocket(sys.argv[1], int(sys.argv[2]))

    sys.stdout.write("waiting for connection...")
    sys.stdout.flush()
    while not(s.accept()): pass
    sys.stdout.write("established.\n")


    while True:
        parts = raw_input("> ").strip().split(" ")

        if parts[0] == "send": 
            s.mysend(" ".join(parts[1:]) + '\n')
        elif parts[0] == "recv":
            print repr(s.myrecv())
        elif parts[0] == "help":
            print "commands: send, recv, help"
        elif parts[0] == "close":
            s.close()
            break
     
