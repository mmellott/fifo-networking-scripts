#!/usr/bin/python
"""
The class contained in this module encapsulates some ugly socket stuff.
"""
import sys
import socket
import errno


class MySocket:
    """ Encapsulates some communication protocol stuff that will make our
    system better. """

    def __init__(self, sock=None):
        """ Construct an IPv4 stream socket or contain sock at input. """
        if sock:
            self._sock = sock
            self._sock.setblocking(0)
        else: self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def connect(self, host, port):
        """ Connect to the host at port. """
        self._sock.connect((host, port))
        self._sock.setblocking(0)

    def mysend(self, msg):
        """ Send all of msg. Throws exception when connection broken. """
        self._sock.send(msg)

    def myrecv(self):
        """ Returns: string data that was received ("" if we should try again).
        Raises: socket.error(32, "Broken pipe") when the connection brakes. """
        try:
            recv = ""
            while True:
                tmp = self._sock.recv(4096)
                recv += tmp
                if len(tmp) != 4096:
                    break

            if recv == "": raise socket.error(32, "Broken pipe")
            else: return recv

        except socket.error as err:
            if err[0] == errno.EAGAIN: return "" # try again
            else: raise err # exceptional excpetion

    def close(self):
        self._sock.close()


if __name__ == "__main__":
    if len(sys.argv) != 3: sys.exit("usage: mysock.py host port") 
    s = MySocket()
    s.connect(sys.argv[1], int(sys.argv[2]))

    while True:
        parts = raw_input("> ").strip().split(" ")

        if parts[0] == "send": 
            s.mysend(" ".join(parts[1:]) + '\n')
        elif parts[0] == "recv":
            print repr(s.myrecv())
        elif parts[0] == "help":
            print "commands: send, recv, help, close"
        elif parts[0] == "close":
            s.close()
            break
        





